﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.Response;
using uab.noticias.Domain.Settings;

namespace B_Agro.Presentacion.WebApi.Controllers.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [AllowAnonymous]
    public class LoginController : ControllerBase
    {

        private readonly IUsers users;
        public LoginController(IUsers users)
        {
            this.users = users;
        }

        [HttpPost]
        public async Task<ResponseEntity> Post([FromBody] AuthSettings user)
        {
            var respuesta = await users.Login(user);
            return respuesta;
        }
    }
}
