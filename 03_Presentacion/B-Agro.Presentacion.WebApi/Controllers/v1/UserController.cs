﻿using Microsoft.AspNetCore.Mvc;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.Response;
using uab.noticias.Domain.Entities;

namespace B_Agro.Presentacion.WebApi.Controllers.v1
{

    public class UserController : BaseApiController
    {
        private readonly IUsers users;
        public UserController(IUsers users)
        {
            this.users = users;
        }

        [HttpGet("{id}")]
        public async Task<ResponseEntity> GetId(int id)
        {
            return await users.GetId(id);
        }

        [HttpGet]
        public async Task<ResponseQuery> Get()
        {
            return await users.GetAll();
        }

        [HttpPost]
        public async Task<ResponseOperation> Post([FromBody] UsuarioRegDto usuario)
        {
            return await users.Add(usuario);
        }

        [HttpPut]
        public async Task<ResponseOperation> Put([FromBody] UsuarioRegDto usuario)
        {
            return await users.Update(usuario);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseOperation> Delete(int id)
        {
            return await users.Delete(id);
        }


    }
}
