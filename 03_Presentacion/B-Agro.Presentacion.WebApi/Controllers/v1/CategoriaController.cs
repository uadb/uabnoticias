﻿using Microsoft.AspNetCore.Mvc;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.Response;

namespace B_Agro.Presentacion.WebApi.Controllers.v1
{

    public class CategoriaController : BaseApiController
    {

        private readonly ICategoria _context;
        public CategoriaController(ICategoria context)
        {
            this._context = context;
        }

        [HttpGet("{id}")]
        public async Task<ResponseEntity> GetId(int id)
        {
            return await _context.GetId(id);
        }

        [HttpGet]
        public async Task<ResponseQuery> Get()
        {
            return await _context.GetAll();
        }

        [HttpPost]
        public async Task<ResponseOperation> Post([FromBody] CategoriaDto _entidad)
        {
            return await _context.Add(_entidad);
        }

        [HttpPut]
        public async Task<ResponseOperation> Put([FromBody] CategoriaDto _entidad)
        {
            return await _context.Update(_entidad);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseOperation> Delete(int id)
        {
            return await _context.Delete(id);
        }

    }
}
