﻿using Microsoft.AspNetCore.Mvc;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.Response;

namespace B_Agro.Presentacion.WebApi.Controllers.v1
{

    public class NoticiaController : BaseApiController
    {
        private readonly INoticia _context;
        private readonly IRssFeed _feed;
        public NoticiaController(INoticia context, IRssFeed feed)
        {
            this._context = context;
            _feed = feed;
        }

        [HttpGet("{id}")]
        public async Task<ResponseEntity> GetId(int id)
        {
            return await _context.GetId(id);
        }

        [HttpGet]
        public async Task<ResponseQuery> Get()
        {
            return await _context.GetAll();
        }

        [HttpPost]
        public async Task<ResponseOperation> Post([FromBody] NoticiaDto _entidad)
        {
            return await _context.Add(_entidad);
        }

        [HttpPost("/rss/feed/{categoria}")]
        public async Task<ResponseOperation> Feed(int categoria)
        {
            return await _feed.GetFeed(categoria);
        }

        [HttpPut]
        public async Task<ResponseOperation> Put([FromBody] NoticiaDto _entidad)
        {
            return await _context.Update(_entidad);
        }

        [HttpDelete("{id}")]
        public async Task<ResponseOperation> Delete(int id)
        {
            return await _context.Delete(id);
        }
    }
}
