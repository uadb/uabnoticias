using B_Agro.Presentacion.WebApi.Extension;
using uab.noticias.Application;
using uab.noticias.Infraestructura.Persistence;
using uab.noticias.Infraestructura.Shared;

var builder = WebApplication.CreateBuilder(args);

//agregando las capas segun arquitectura oniomn
builder.Services.AddApplicationLayer();
builder.Services.AddPersistenceInfrastructure(builder.Configuration);
builder.Services.AddSharedInfrastructure(builder.Configuration);
builder.Services.AddSwaggerExtension();
builder.Services.AddControllersExtension();

builder.Services.AddCorsExtension();
builder.Services.AddApiVersioningExtension();
//builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();
builder.Services.AddVersionedApiExplorerExtension();
builder.Services.AddJWTAuthentication(builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();


app.Run();
