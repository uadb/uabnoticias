﻿using Microsoft.EntityFrameworkCore;
using uab.noticias.Domain;
using uab.noticias.Domain.Entities;

namespace uab.noticias.Infraestructura.Persistence.Context
{
    public class GenericContext : DbContext
    {
        public GenericContext(DbContextOptions options) : base(options) { }


        //TODO: 001 - Crear Entidades en la Carpete Entities
        //TODO: 002 - Registrar DBSET en GenericDataContext

        public DbSet<SegUsuarios> Usuarios { get; set; }
        public DbSet<NtcCategoria> Categorias { get; set; }
        public DbSet<NtcNoticia> Noticias { get; set; }

    }
}
