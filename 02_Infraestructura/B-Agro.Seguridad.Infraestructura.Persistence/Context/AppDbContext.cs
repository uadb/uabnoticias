﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uab.noticias.Application.Interfaces;
using uab.noticias.Domain.Common;

namespace uab.noticias.Infraestructura.Persistence.Context;


public sealed class AppDbContext : GenericContext
{
    private readonly IDateTimeService _dateTime;
    private readonly ICurrentUserService _user;

    public AppDbContext(DbContextOptions<AppDbContext> options, IDateTimeService dateTime, ICurrentUserService user) :
        base(options)
    {
        ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        _dateTime = dateTime;
        _user = user;
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        foreach (var entry in ChangeTracker.Entries<AuditableBaseEntity>())
        {
            switch (entry.State)
            {
                case EntityState.Modified:
                    entry.Entity.UsuarioModificacion = _user.GetUserSettings().Login;
                    entry.Entity.FechaModificacion = _dateTime.NowUtc;
                    entry.Property(x => x.FechaCreacion).IsModified = false;
                    entry.Property(x => x.UsuarioCreacion).IsModified = false;
                    break;
                case EntityState.Added:
                    entry.Entity.UsuarioCreacion = _user.GetUserSettings().Login;
                    entry.Entity.FechaCreacion = _dateTime.NowUtc;
                    entry.Property(x => x.FechaModificacion).IsModified = false;
                    entry.Property(x => x.UsuarioModificacion).IsModified = false;
                    break;
                case EntityState.Detached:
                    break;
                case EntityState.Unchanged:
                    break;
                case EntityState.Deleted:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var propAttr = entry.Entity.GetType()
                .GetProperties()
                .ToList().Where(prop => prop.IsDefined(typeof(IsUpperCase), false))
                .ToList();

            foreach (var t in propAttr)
            {
                var value = entry.Entity
                    .GetType()
                    .GetProperty(t.Name)!
                    .GetValue(entry.Entity) != null
                    ? entry.Entity.GetType()
                        .GetProperty(t.Name)!
                        .GetValue(entry.Entity)!.ToString()!.ToUpper()
                    : null;
                if (value != null)
                {
                    entry.Entity.GetType()
                        .GetProperty(t.Name)!
                        .SetValue(entry.Entity, value);
                }
            }
        }

        return base.SaveChangesAsync(cancellationToken);
    }
}