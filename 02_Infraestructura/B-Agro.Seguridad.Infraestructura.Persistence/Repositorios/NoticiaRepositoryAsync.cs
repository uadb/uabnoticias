﻿using uab.noticias.Application.IRepositories;
using uab.noticias.Domain;
using uab.noticias.Infraestructura.Persistence.Context;

namespace uab.noticias.Infraestructura.Persistence.Repositorios
{
    public class NoticiaRepositoryAsync : GenericRepositoryAsync<NtcNoticia>, INoticiaRepositoryAsycn
    {

        private readonly AppDbContext _dbContext;
        public NoticiaRepositoryAsync(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
