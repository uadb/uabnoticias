﻿using Microsoft.EntityFrameworkCore;
using uab.noticias.Application.IRepositories;
using uab.noticias.Application.Response;
using uab.noticias.Domain.Entities;
using uab.noticias.Domain.Settings;
using uab.noticias.Infraestructura.Persistence.Context;

namespace uab.noticias.Infraestructura.Persistence.Repositorios
{
    public class UsuarioRepositoryAsync : GenericRepositoryAsync<SegUsuarios>, IUsuarioRepositoryAsync
    {
        private readonly AppDbContext _dbContext;

        public UsuarioRepositoryAsync(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ResponseEntity> Login(AuthSettings requestParameter)
        {
            var _resp = new ResponseEntity();
            try
            {               
                var entidad = await _dbContext.Usuarios.Where(x => x.Login == requestParameter.UserName && x.Password == requestParameter.Password).FirstOrDefaultAsync();

                _resp.State = entidad != null ? Application.Enums.State.Success : Application.Enums.State.NoData;
                _resp.Data = entidad != null ? entidad : null;
                _resp.Message = entidad != null ? Recursos.Generic.EntitySuccess : Recursos.Generic.EntityNoData;
            }
            catch (Exception e)
            {
                _resp.State = Application.Enums.State.Error;
                _resp.Data = null;
                _resp.Message = Recursos.Generic.EntityError;
            }

            return _resp;
        }
    }
}
