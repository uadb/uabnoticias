﻿using uab.noticias.Application.IRepositories;
using uab.noticias.Domain;
using uab.noticias.Infraestructura.Persistence.Context;

namespace uab.noticias.Infraestructura.Persistence.Repositorios
{
    public class CategoriaRepositoryAsync : GenericRepositoryAsync<NtcCategoria>, ICategoriaRepositoryAsync
    {
        private readonly AppDbContext _dbContext;

        public CategoriaRepositoryAsync(AppDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

    }
}
