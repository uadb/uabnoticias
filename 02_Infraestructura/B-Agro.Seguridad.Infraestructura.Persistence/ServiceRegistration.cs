﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using uab.noticias.Application.Interfaces;
using uab.noticias.Application.IRepositories;
using uab.noticias.Infraestructura.Persistence.Context;
using uab.noticias.Infraestructura.Persistence.Repositorios;

namespace uab.noticias.Infraestructura.Persistence
{
    public static class ServiceRegistration
    {
        public static void AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options =>
                                            options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"))
                                                    .UseSnakeCaseNamingConvention());

            //TODO: exponer repositorios
            #region Repositories
            services.AddTransient(typeof(IGenericRepositoryAsync<>), typeof(GenericRepositoryAsync<>));
            services.AddTransient<IUsuarioRepositoryAsync, UsuarioRepositoryAsync>();
            services.AddTransient<ICategoriaRepositoryAsync, CategoriaRepositoryAsync>();
            services.AddTransient<INoticiaRepositoryAsycn, NoticiaRepositoryAsync>();
            #endregion Repositories

        }
    }
}
