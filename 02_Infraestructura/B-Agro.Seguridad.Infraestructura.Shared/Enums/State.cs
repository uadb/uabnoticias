﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uab.noticias.Infraestructura.Shared.Enums
{
    public enum State
    {
        Success = 1,
        Warning = 2,
        Error = 3,
        NoData = 4
    }
}
