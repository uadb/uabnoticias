﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using uab.noticias.Application.Interfaces;
using uab.noticias.Domain.Settings;

namespace uab.noticias.Infraestructura.Shared.Implements
{
    public class TokenService : ITokenService
    {

        private readonly IConfiguration _conf;
        public TokenService(IConfiguration conf)
        {
            _conf = conf;
        }

        public string BuildToken(UserSettings user)
        {
            TimeSpan ExpiryDuration = new TimeSpan(0, Int32.Parse(_conf["Jwt:Duration"]), 0);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Login),
                new Claim(ClaimTypes.Name, user.Nombres),
                new Claim("ApPaterno", user.ApPaterno),
                new Claim("ApMaterno", user.ApMaterno),
                new Claim("IdsegUsuarios", user.IdsegUsuarios.ToString()),
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_conf["Jwt:key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var tokenDescriptor = new JwtSecurityToken(_conf["Jwt:Issuer"], _conf["Jwt:Issuer"], claims, expires : DateTime.Now.Add(ExpiryDuration), signingCredentials: credentials);
            
            return new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);

        }
    }
}
