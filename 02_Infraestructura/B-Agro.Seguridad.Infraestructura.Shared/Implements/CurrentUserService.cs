﻿using Microsoft.AspNetCore.Http;
using uab.noticias.Application.Interfaces;
using uab.noticias.Domain.Settings;

namespace uab.noticias.Infraestructura.Share.Implements
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _accessor;
        public CurrentUserService(IHttpContextAccessor accessor)
        {
            this._accessor = accessor;
        }

        public UserSettings GetUserSettings()
        {
            var vUser = new UserSettings();

            if (_accessor.HttpContext.User.Claims.ToList().Count <= 0) return vUser;

            vUser.Login = _accessor.HttpContext.User.Claims.ToList().FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"))?.Value;
            vUser.Nombres = _accessor.HttpContext.User.Claims.ToList().FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"))?.Value;
            vUser.IdsegUsuarios = Convert.ToInt32(_accessor.HttpContext.User.FindFirst("IdsegUsuarios")!.Value);
            vUser.ApMaterno = _accessor.HttpContext.User.FindFirst("ApMaterno")!.Value;
            vUser.ApMaterno = _accessor.HttpContext.User.FindFirst("ApMaterno")!.Value;


            return vUser;
        }
    }
}