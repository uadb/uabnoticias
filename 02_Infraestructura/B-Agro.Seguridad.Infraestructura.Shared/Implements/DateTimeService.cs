﻿using uab.noticias.Application.Interfaces;

namespace uab.noticias.Infraestructura.Shared.Implements
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime NowUtc => DateTime.UtcNow;
    }
}