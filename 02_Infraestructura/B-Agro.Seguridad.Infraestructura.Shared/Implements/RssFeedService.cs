﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uab.noticias.Application.Interfaces;
using uab.noticias.Application.Response;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Xml.Schema;

namespace uab.noticias.Infraestructura.Shared.Implements
{
    public class RssFeedService : IRssFeedService
    {
        public ResponseOperation GetRssFeed(string Url)
        {
            var _Respuesta = new ResponseOperation();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            using var reader = XmlReader.Create(Url, settings);
            var feed = SyndicationFeed.Load(reader);
            var post = feed.Items.FirstOrDefault();

            return _Respuesta;
        }


        private static void ValidationCallBack(object sender, ValidationEventArgs e)
        {
            Console.WriteLine("Validation Error: {0}", e.Message);
        }
    }
}
