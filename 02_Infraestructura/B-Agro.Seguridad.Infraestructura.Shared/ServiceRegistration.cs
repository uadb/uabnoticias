﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using uab.noticias.Application.Interfaces;
using uab.noticias.Infraestructura.Share.Implements;
using uab.noticias.Infraestructura.Shared.Implements;

namespace uab.noticias.Infraestructura.Shared
{
    public static class ServiceRegistration
    {
        public static void AddSharedInfrastructure(this IServiceCollection services, IConfiguration _config)
        {

           
            services.AddTransient<ICurrentUserService, CurrentUserService>();
            services.AddTransient<IDateTimeService, DateTimeService>();
            services.AddTransient<ITokenService, TokenService>();
            services.AddTransient<IRssFeedService, RssFeedService>();
        }
    }
}
