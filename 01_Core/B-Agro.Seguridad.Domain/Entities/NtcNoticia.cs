using uab.noticias.Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace uab.noticias.Domain
{

    [Table("ntc_noticia", Schema = "public")]
    public class NtcNoticia : AuditableBaseEntity
    {
        [Key]
        public int IdntcNoticia { get; set; }
        public int? IdntcCategoria { get; set; }
        [MaxLength(150)]
        public string? Title { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        [MaxLength(3000)]
        public string? Content { get; set; }
        [MaxLength(3000)]
        public string? Imagen { get; set; }

    }
}
