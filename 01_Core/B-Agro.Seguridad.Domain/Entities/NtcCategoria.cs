using uab.noticias.Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace uab.noticias.Domain
{

    [Table("ntc_categoria", Schema = "public")]
    public class NtcCategoria : AuditableBaseEntity
    {
        [Key]
        public int IdntcCategoria { get; set; }
        [MaxLength(3000)]
        public string? Title { get; set; }
        [MaxLength(3000)]
        public string? Link { get; set; }
        [MaxLength(3000)]
        public string? Description { get; set; }
        [MaxLength(3000)]
        public string? Fuente { get; set; }

    }
}
