using uab.noticias.Domain.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace uab.noticias.Domain.Entities
{

    [Table("seg_usuarios", Schema = "public")]
    public class SegUsuarios : AuditableBaseEntity
    {
        [Key]
        public int IdsegUsuarios { get; set; }
        [MaxLength(20)]
        [IsUpperCase]
        public string? Nombres { get; set; }
        [MaxLength(20)]
        [IsUpperCase]
        public string? ApPaterno { get; set; }
        [MaxLength(20)]
        [IsUpperCase]
        public string? ApMaterno { get; set; }
        [MaxLength(20)]
        public string? Login { get; set; }
        [MaxLength(300)]
        public string? Password { get; set; }


    }
}
