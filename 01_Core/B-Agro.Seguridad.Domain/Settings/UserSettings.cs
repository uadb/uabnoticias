﻿namespace uab.noticias.Domain.Settings
{
    public class UserSettings
    {
        public int IdsegUsuarios { get; set; }
        public string? Nombres { get; set; }
        public string? ApPaterno { get; set; }
        public string? ApMaterno { get; set; }
        public string? Login { get; set; }

    }
}
