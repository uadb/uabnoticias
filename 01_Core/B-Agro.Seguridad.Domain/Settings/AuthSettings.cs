﻿namespace uab.noticias.Domain.Settings
{
    public class AuthSettings
    {
        public string? UserName { get; set; }
        public string? Password { get; set; }
    }
}
