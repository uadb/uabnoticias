﻿using uab.noticias.Application.Interfaces;
using uab.noticias.Application.Response;
using uab.noticias.Domain.Entities;
using uab.noticias.Domain.Settings;

namespace uab.noticias.Application.IRepositories
{
    public  interface IUsuarioRepositoryAsync : IGenericRepositoryAsync<SegUsuarios>
    {
        Task<ResponseEntity> Login(AuthSettings requestParameter);

    }
}
