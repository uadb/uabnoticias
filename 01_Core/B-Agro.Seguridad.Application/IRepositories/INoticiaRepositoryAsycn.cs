﻿using uab.noticias.Application.Interfaces;
using uab.noticias.Domain;

namespace uab.noticias.Application.IRepositories
{
    public interface INoticiaRepositoryAsycn : IGenericRepositoryAsync<NtcNoticia>
    {

    }
}
