﻿namespace uab.noticias.Application.Dtos
{
    public class CategoriaDto
    {
        public int IdntcCategoria { get; set; }
        public string? Title { get; set; }
        public string? Link { get; set; }
        public string? Description { get; set; }
        public string? Fuente { get; set; }
    }

}
