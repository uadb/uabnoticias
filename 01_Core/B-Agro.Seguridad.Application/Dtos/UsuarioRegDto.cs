﻿namespace uab.noticias.Application.Dtos
{
    public class UsuarioRegDto
    {
        public int IdsegUsuarios { get; set; }
        public string? Nombres { get; set; }
        public string? ApPaterno { get; set; }
        public string? ApMaterno { get; set; }
        public string? Login { get; set; }
        public string? Password { get; set; }

    }
}
