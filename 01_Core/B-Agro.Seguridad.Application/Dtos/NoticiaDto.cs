﻿namespace uab.noticias.Application.Dtos
{
    public class NoticiaDto
    {
        public int IdntcNoticia { get; set; }
        public int? IdntcCategoria { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? Content { get; set; }
        public string? Imagen { get; set; }

    }
}
