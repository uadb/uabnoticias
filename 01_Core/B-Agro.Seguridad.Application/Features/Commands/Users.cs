﻿using AutoMapper;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Enums;
using uab.noticias.Application.Helpers;
using uab.noticias.Application.Interfaces;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.IRepositories;
using uab.noticias.Application.Response;
using uab.noticias.Domain.Entities;
using uab.noticias.Domain.Settings;

namespace uab.noticias.Application.Features.Commands
{
    public class Users : IUsers
    {
        private readonly IUsuarioRepositoryAsync _repository;
        private readonly ITokenService _token;
        private readonly IMapper _mapp;

        public Users(IUsuarioRepositoryAsync repository, ITokenService token, IMapper mapp)
        {
            _repository = repository;
            _token = token;
            _mapp = mapp;
        }

        public async Task<ResponseEntity> Login(AuthSettings req)
        {

            req.Password = cEncryp.Md5Hash(req.Password).ToLower().ToString();
            var _resp = await _repository.Login(req);

            try
            {
                if (_resp.State != Enums.State.Success)
                    throw new Exception("Usuario o Password Incorrectos.");

                UserSettings user = new UserSettings
                {
                    IdsegUsuarios = ((SegUsuarios)_resp.Data).IdsegUsuarios,
                    Nombres = ((SegUsuarios)_resp.Data).Nombres,
                    ApPaterno = ((SegUsuarios)_resp.Data).ApPaterno,
                    ApMaterno = ((SegUsuarios)_resp.Data).ApMaterno,
                    Login = ((SegUsuarios)_resp.Data).Login
                };

                var strToken = _token.BuildToken(user);

                if (string.IsNullOrEmpty(strToken))
                    throw new Exception("Usuario o Password Incorrectos.");

                ((SegUsuarios)_resp.Data).Password = strToken;

            }
            catch (Exception e)
            {
                _resp.Message = e.Message;
                _resp.State = Enums.State.Error;
                _resp.Data = null;
            }

            return _resp;

        }

        public async Task<ResponseEntity> GetId(int id)
        {
            var _resp = new ResponseEntity();            
            var entUsuario = await _repository.GetByIdAsync(id);
            _resp.Data = _mapp.Map<UsuarioDto>(entUsuario);            
            return _resp;
        }

        public async Task<ResponseQuery> GetAll()
        {
            var _resp = new ResponseQuery();
            var lstEntity = await _repository.GetAllAsync();
            _resp.Data = _mapp.Map<List<UsuarioDto>>(lstEntity);
            return _resp;
        }

        public async Task<ResponseOperation> Add(UsuarioRegDto usu)
        {
            var _respuesta = new ResponseOperation();
            var EntityUsuario = _mapp.Map<SegUsuarios>(usu);
            EntityUsuario.Password = cEncryp.Md5Hash(EntityUsuario.Password).ToLower().ToString(); 
            _respuesta.Data = await _repository.AddAsync(EntityUsuario);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            
            return _respuesta;
        }

        public async Task<ResponseOperation> Update(UsuarioRegDto usu)
        {
            var _respuesta = new ResponseOperation();

            var EntityUsuario = _mapp.Map<SegUsuarios>(usu);
            EntityUsuario.Password = cEncryp.Md5Hash(EntityUsuario.Password).ToLower().ToString();
            _respuesta.Data = await _repository.UpdateAsync(EntityUsuario);

            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            
            return _respuesta;
        }

        public async Task<ResponseOperation> Delete(int id)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = await _repository.GetByIdAsync(id);
            _respuesta.Data = await _repository.DeleteAsync(vEntity);

            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }
    }
}
