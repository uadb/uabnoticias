﻿using AutoMapper;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Enums;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.IRepositories;
using uab.noticias.Application.Response;
using uab.noticias.Domain;

namespace uab.noticias.Application.Features.Commands
{
    public class Categoria : ICategoria
    {
        private readonly ICategoriaRepositoryAsync _repository;
        private readonly IMapper _mapp;
        public Categoria(ICategoriaRepositoryAsync repository,IMapper mapp)
        {
            _repository = repository;            
            _mapp = mapp;
        }

        public async Task<ResponseEntity> GetId(int id)
        {
            var _resp = new ResponseEntity();
            var entCategoria = await _repository.GetByIdAsync(id);
            _resp.Data = _mapp.Map<CategoriaDto>(entCategoria);
            return _resp;
        }

        public async Task<ResponseQuery> GetAll()
        {
            var _resp = new ResponseQuery();
            var lstEntity = await _repository.GetAllAsync();
            _resp.Data = _mapp.Map<List<CategoriaDto>>(lstEntity);
            return _resp;
        }

        public async Task<ResponseOperation> Add(CategoriaDto _entidad)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = _mapp.Map<NtcCategoria>(_entidad);
            _respuesta.Data = await _repository.AddAsync(vEntity);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }

        public async Task<ResponseOperation> Update(CategoriaDto _entiad)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = _mapp.Map<NtcCategoria>(_entiad);            
            _respuesta.Data = await _repository.UpdateAsync(vEntity);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }

        public async Task<ResponseOperation> Delete(int id)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = await _repository.GetByIdAsync(id);
            _respuesta.Data = await _repository.DeleteAsync(vEntity);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }
    }
}
