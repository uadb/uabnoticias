﻿

using AutoMapper;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Enums;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.IRepositories;
using uab.noticias.Application.Response;
using uab.noticias.Domain;

namespace uab.noticias.Application.Features.Commands
{
    public class Noticia : INoticia
    {
        private readonly INoticiaRepositoryAsycn _repository;
        private readonly IMapper _mapp;

        public Noticia(INoticiaRepositoryAsycn repository, IMapper mapp)
        {
            _repository = repository;
            _mapp = mapp;
        }

        public async Task<ResponseEntity> GetId(int id)
        {
            var _resp = new ResponseEntity();
            var entCategoria = await _repository.GetByIdAsync(id);
            _resp.Data = _mapp.Map<NoticiaDto>(entCategoria);
            return _resp;
        }

        public async Task<ResponseQuery> GetAll()
        {
            var _resp = new ResponseQuery();
            var lstEntity = await _repository.GetAllAsync();
            _resp.Data = _mapp.Map<List<NoticiaDto>>(lstEntity);
            return _resp;
        }

        public async Task<ResponseOperation> Add(NoticiaDto _entidad)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = _mapp.Map<NtcNoticia>(_entidad);
            _respuesta.Data = await _repository.AddAsync(vEntity);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }

        public async Task<ResponseOperation> Update(NoticiaDto _entiad)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = _mapp.Map<NtcNoticia>(_entiad);
            _respuesta.Data = await _repository.UpdateAsync(vEntity);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }

        public async Task<ResponseOperation> Delete(int id)
        {
            var _respuesta = new ResponseOperation();
            var vEntity = await _repository.GetByIdAsync(id);
            _respuesta.Data = await _repository.DeleteAsync(vEntity);
            _respuesta.Message = _respuesta.Data == null ? "" : "";
            _respuesta.State = _respuesta.Data == null ? State.NoData : State.Success;
            return _respuesta;
        }


    }
}
