﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uab.noticias.Application.Interfaces;
using uab.noticias.Application.Interfaces.Features;
using uab.noticias.Application.IRepositories;
using uab.noticias.Application.Response;

namespace uab.noticias.Application.Features.Commands
{
    public class RssFeed : IRssFeed
    {
        private readonly ICategoriaRepositoryAsync _repository;
        private readonly IRssFeedService _rss;
        public RssFeed(ICategoriaRepositoryAsync repository, IRssFeedService rss)
        {
            _repository = repository;
            _rss = rss;
        }

        public async Task<ResponseOperation> GetFeed(int idCategoria)
        {
            var _respuesta = new ResponseOperation();
            var _vEntCategoria = await _repository.GetByIdAsync(idCategoria);
            var feed = _rss.GetRssFeed(_vEntCategoria.Link);

            return _respuesta;
        }
    }
}
