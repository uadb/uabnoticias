﻿using uab.noticias.Applicationn.Response;

namespace uab.noticias.Application.Response
{
    public class ResponseOperation : ResponseBase
    {
        public Object? Data { get; set; }
    }
}
