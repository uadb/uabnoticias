﻿using uab.noticias.Application.Enums;

namespace uab.noticias.Applicationn.Response
{
    public class ResponseBase
    {
        public State State { get; set; }
        public string? Message { get; set; }

    }
}
