﻿using uab.noticias.Applicationn.Response;

namespace uab.noticias.Application.Response
{
    public class ResponseEntity : ResponseBase
    {
        public Object? Data { get; set; }

    }
}
