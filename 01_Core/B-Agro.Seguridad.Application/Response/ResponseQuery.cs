﻿using System.Collections.Generic;
using uab.noticias.Applicationn.Response;

namespace uab.noticias.Application.Response
{
    public class ResponseQuery : ResponseBase
    {
        public IEnumerable<Object>? Data { get; set; }
    }
}
