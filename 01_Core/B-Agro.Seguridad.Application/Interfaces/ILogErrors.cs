﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uab.noticias.Application.Interfaces
{
    internal interface ILogErrors
    {
        void Register(Exception e);
    }
}
