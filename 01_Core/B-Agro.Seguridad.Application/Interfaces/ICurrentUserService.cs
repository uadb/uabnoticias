﻿using uab.noticias.Domain.Settings;

namespace uab.noticias.Application.Interfaces
{
    public interface ICurrentUserService
    {
        UserSettings GetUserSettings();
    }
}
