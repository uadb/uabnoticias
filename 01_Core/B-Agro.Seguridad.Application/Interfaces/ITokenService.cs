﻿using uab.noticias.Domain.Settings;

namespace uab.noticias.Application.Interfaces
{
    public interface ITokenService
    {
        string BuildToken(UserSettings user);
    }
}
