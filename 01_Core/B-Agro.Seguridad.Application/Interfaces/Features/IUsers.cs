﻿using uab.noticias.Application.Dtos;
using uab.noticias.Application.Response;
using uab.noticias.Domain.Settings;

namespace uab.noticias.Application.Interfaces.Features
{
    public interface IUsers
    {
        Task<ResponseEntity> Login(AuthSettings req);
 
        Task<ResponseEntity> GetId(int Id);
        Task<ResponseQuery> GetAll();
        Task<ResponseOperation> Add(UsuarioRegDto usu);
        Task<ResponseOperation> Update(UsuarioRegDto usu);
        Task<ResponseOperation> Delete(int id);
    }
}
