﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uab.noticias.Application.Response;

namespace uab.noticias.Application.Interfaces.Features
{
    public interface IRssFeed
    {
       Task<ResponseOperation> GetFeed(int i);
    }
}
