﻿using uab.noticias.Application.Dtos;
using uab.noticias.Application.Response;

namespace uab.noticias.Application.Interfaces.Features
{
    public interface ICategoria 
    {
        Task<ResponseEntity> GetId(int Id);
        Task<ResponseQuery> GetAll();
        Task<ResponseOperation> Add(CategoriaDto _entidad);
        Task<ResponseOperation> Update(CategoriaDto _entiad);
        Task<ResponseOperation> Delete(int id);
    }
}
