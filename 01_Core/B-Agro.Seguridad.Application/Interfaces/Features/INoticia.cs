﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uab.noticias.Application.Dtos;
using uab.noticias.Application.Response;

namespace uab.noticias.Application.Interfaces.Features
{
    public interface INoticia
    {

        Task<ResponseEntity> GetId(int Id);
        Task<ResponseQuery> GetAll();
        Task<ResponseOperation> Add(NoticiaDto _entidad);
        Task<ResponseOperation> Update(NoticiaDto _entiad);
        Task<ResponseOperation> Delete(int id);

    }
}
