﻿using AutoMapper;
using uab.noticias.Application.Dtos;
using uab.noticias.Domain;
using uab.noticias.Domain.Entities;

namespace uab.noticias.Application.Mapping
{
    public class GeneralMappings : Profile
    {
        public GeneralMappings()
        {
            #region QueryDto
            CreateMap<SegUsuarios, UsuarioDto>();
            CreateMap<NtcCategoria, CategoriaDto>();            
            CreateMap<NtcNoticia, NoticiaDto>();            
            #endregion

            #region Commands
            CreateMap<UsuarioDto, SegUsuarios>();
            CreateMap<UsuarioRegDto, SegUsuarios>();
            CreateMap<NoticiaDto, NtcNoticia>();
            CreateMap<CategoriaDto, NtcCategoria>();
            #endregion

        }
    }
}
