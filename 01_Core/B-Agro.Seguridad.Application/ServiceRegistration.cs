﻿using Microsoft.Extensions.DependencyInjection;
using uab.noticias.Application.Features.Commands;
using uab.noticias.Application.Interfaces.Features;

namespace uab.noticias.Application
{
    public static class ServiceRegistration
    {

        public static void AddApplicationLayer(this IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());


            //TODO: 004 - Crear la interfaz en la carpeta Interfaces
            //TODO: 005 - Crear el servicio de implementacion enn la carpeta features
            //TODO: 006 - Registrar la iyeccion de dependencias ejemplo aqui ..
            services.AddScoped<IUsers, Users>();
            services.AddScoped<ICategoria, Categoria>();
            services.AddScoped<INoticia, Noticia>();
            services.AddScoped<IRssFeed, RssFeed>();
        }


    }
}
